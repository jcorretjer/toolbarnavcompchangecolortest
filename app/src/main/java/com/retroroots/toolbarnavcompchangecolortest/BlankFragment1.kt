package com.retroroots.toolbarnavcompchangecolortest

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

class BlankFragment1 : Fragment(R.layout.fragment_blank1)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.toBtn).setOnClickListener {
            findNavController().navigate(R.id.blankFragment2)
        }
    }
}